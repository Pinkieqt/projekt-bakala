import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './_components/nav-menu/nav-menu.component';
import { HomeComponent } from './_components/home/home.component';
import { RegisterComponent } from './_components/register/register.component';
import { EditingUserComponent } from './_components/editingUser/editingUser.component';
import { FetchusersComponent } from './_components/fetchusers/fetchusers.component';
import { UserService } from './_services/user.service';
import { CommentService } from './_services/comment.service';
import { LoginComponent } from './_components/login/login.component';
import { AuthorizationService } from './_services/auth.service';
import { ProjectService } from './_services/project.service';
import { AuthenticationGuard } from './_guards/authentication.guard';
import { JwtHelper } from 'angular2-jwt';
import { ProjectCreateComponent } from './_components/projectCreate/projectCreate.component';
import { SidebarComponent } from './_components/sidebar/sidebar.component';
import { ProjectComponent } from './_components/project/project.component';
import { NavBarComponent } from './_components/nav-bar/nav-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    RegisterComponent,
    FetchusersComponent,
    LoginComponent,
    EditingUserComponent,
    ProjectCreateComponent,
    SidebarComponent,
    ProjectComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, canActivate: [AuthenticationGuard], pathMatch: 'full' }, //Login
      { path: 'register', component: RegisterComponent, canActivate: [AuthenticationGuard] },
      { path: 'fetch', component: FetchusersComponent, canActivate: [AuthenticationGuard] },
      { path: 'project_create', component: ProjectCreateComponent, canActivate: [AuthenticationGuard] },
      { path: 'project/:id', component: ProjectComponent, canActivate: [AuthenticationGuard] },
      { path: 'project/:id/:taskId', component: ProjectComponent, canActivate: [AuthenticationGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'edit_user/:login', component: EditingUserComponent, canActivate: [AuthenticationGuard] }
    ])
  ],
  providers: [UserService, AuthorizationService, ProjectService, AuthenticationGuard, JwtHelper, CommentService],
  bootstrap: [AppComponent]
})
export class AppModule { }

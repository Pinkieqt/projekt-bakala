import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{
  //Form
  private userForm: FormGroup;
  private title: string = 'Login';
  private authorizedLogin: boolean = true;
  public unvalidFormWarning: string = ".";


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthorizationService,
    private router: Router,
  ) {
    this.userForm = this.formBuilder.group({
      Login: ['', [Validators.required, Validators.maxLength(30)]],
      Password: ['', [Validators.required, Validators.maxLength(32)]]
    })
  }

  login() {
    if (!this.userForm.valid) {
      return;
    }
    this.authService.loginUser(this.userForm.value)
      .subscribe(response => {
        let token = (<any>response).token;
        localStorage.setItem("jwt", token);
        localStorage.setItem("log", this.userForm.controls['Login'].value);
        this.authorizedLogin = true;
        this.unvalidFormWarning = ".";
        this.router.navigate([""]);
      }, error => {
        this.authorizedLogin = false;
        this.unvalidFormWarning = "Invalid username or password.";
        alert("Invalid username or password.");
      })
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../_services/user.service';
import { DISABLED } from '@angular/forms/src/model';

@Component({
  selector: 'app-editingUser',
  templateUrl: './editingUser.component.html',
  styleUrls: ['./editingUser.component.css']
})
export class EditingUserComponent {

  userForm: FormGroup;
  private userList: any;
  tmpLogin: string;
  private tmpfName: string;
  private tmplName: string;
  private tmpEmail: string;
  errorMessage: any;
  editingForm: boolean = false;
  invalidForm: boolean = false;


  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.userForm = this.formBuilder.group({
      login: (['', [Validators.required, Validators.minLength(4)]]),
      password: ['', [Validators.required, Validators.minLength(6)]],
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]]
    })

    this.userForm.get("login").disable();
    this.userForm.get("password").disable();
    this.getUser();

  }

  getUser() {
    if (this.activatedRoute.snapshot.params["login"]) {
      this.tmpLogin = this.activatedRoute.snapshot.params["login"];
    }

    if (this.tmpLogin != null) {
      this.userService.getUserByLogin(this.tmpLogin).subscribe(data => {
        this.userList = data;
        this.tmpfName = this.userList[0].first_name;
        this.tmplName = this.userList[0].last_name;
        this.tmpEmail = this.userList[0].email;

        this.userForm.setValue({
          login: this.tmpLogin,
          password: this.userList[0].password,
          first_name: this.tmpfName,
          last_name: this.tmplName,
          email: this.tmpEmail
        });
      });
    }
  }

  editUser() {
    if (!this.userForm.valid) {
      this.invalidForm = true;
      return;
    }
    this.userForm.get("login").enable();
    this.userForm.get("password").enable();
    this.userService.editUserx(this.userForm.value)
      .subscribe((data) => {
        this.editingForm = true;
        this.router.navigate(['/fetch']);
      }, error => {
        this.userForm.get("login").disable();
        this.userForm.get("password").disable();
        this.invalidForm = true;
        this.errorMessage = error;
      })

  }


}

export class UserData {
  id: number;
  login: string;
  password: string;
  first_name: string;
  last_name: string;
  email: string;
}

import { Component } from '@angular/core';
import { AuthorizationService } from '../../_services/auth.service';
import { Router } from '@angular/router';
import { ProjectService } from '../../_services/project.service';
import { UserService } from '../../_services/user.service';


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent
{
  
  loggedIn: boolean = false;
  private tmpData: any;
  projectList: any;
  sharedProjectList: any;
  private currentId: number = null;
  private currentLogin: string;
  private isVisible: boolean = false;

  constructor(
    private authService: AuthorizationService,
    private userService: UserService,
    private projectService: ProjectService,
    router: Router,
  )
  {
    router.events.subscribe((val) =>
    {
      if(!this.loggedIn){
        if (authService.isLoggedIn())
        {
          this.loggedIn = true;
          if (this.currentId == null)
          {
            this.currentId = 1;
            this.getUserId();
          }
        }
        else
        {
          this.loggedIn = false;
        }
      }
    })
  }
  
  //Getting user id based on login
  getUserId()
  {
    this.userService.getUsers().subscribe(data =>
    {
      this.tmpData = data;
      this.currentLogin = localStorage.getItem("log");
      for (let entry of this.tmpData)
      {
        if (entry.login == this.currentLogin)
        {
          localStorage.setItem("userId", entry.id);
          this.currentId = entry.id;
        }
      }
    })
  }


  //Get projects based on userid so he can see it in navbar
  getProjects()
  {
    this.isVisible = !this.isVisible;
    if(this.isVisible == false)
    {
      this.projectList = null;
      this.sharedProjectList = null;
    }
    else{
      this.projectService.getProjectsByUserId(this.currentId).subscribe((data) =>
      {
        if (data != null) this.projectList = data;
      })
      this.projectService.getProjectsByParticipantUserId(this.currentId).subscribe((data =>
      {
        if (data != null) this.sharedProjectList = data;
      }))
    }
  }

  
  //default
  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  logOut()
  {
    this.projectList = [];
    this.sharedProjectList = [];
    this.currentLogin = null;
    this.currentId = null;
    this.loggedIn = false;
    this.authService.logout();
  }
}

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../_services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  //Form
  userForm: FormGroup;
  errorMessage: any;
  invalidForm: boolean = false;
  registeringForm: boolean = false;
  

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
  ) {
    
    this.userForm = this.formBuilder.group({
      login: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(32)]],
      first_name: ['', [Validators.required, Validators.maxLength(30)]],
      last_name: ['', [Validators.required, Validators.maxLength(30)]],
      email: ['', [Validators.required, Validators.email, Validators.maxLength(30)]],
      role: ['', [Validators.required]]
    })  }
  

  register() {
    if (!this.userForm.valid) {
      this.invalidForm = true;
      return;
    }
    this.userService.saveUser(this.userForm.value)
      .subscribe((data) => {
        this.registeringForm = true;
        this.router.navigate(['/fetch']);
        alert("Uživatel byl přidán.");
      }, error => {
        this.invalidForm = true;
        this.errorMessage = error;
      })
  } 
}

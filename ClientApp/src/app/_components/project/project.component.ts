import { CommentService } from './../../_services/comment.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProjectService } from '../../_services/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  
  private tmpProjectId: number;
  private tmpProject: any = {};
  private isOwner: boolean;
  private isEditing: boolean = false;
  private tmpUserId: number = +localStorage.getItem("userId");
  private tmpTask: any = null;
  private tmpTaskId: number;
  private tmpCommentsList: any = null;
  private isVisible: boolean = false;
  private taskForm: FormGroup;
  private commentForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService,
    private commentService: CommentService,
    private router: Router
  ){
    this.taskForm = this.formBuilder.group
    ({
      Name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
      Description: ['', [Validators.required]],
      Fk_Owner_Id: [''],
      Fk_Project_Id: ['']
    })
    this.commentForm = this.formBuilder.group
    ({
      Content: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
      TimeChanged: [''],
      Fk_Owner_Id: [''],
      Fk_Task_Id: ['']
    })
   }

  ngOnInit()
  {
    //dodělat routing, pokud uživatel zadá /project/... bez ID
    this.activatedRoute.params.subscribe(params =>
    {
      this.tmpProjectId = params['id'];
      localStorage.setItem("projectId", params['id']);
      this.tmpTaskId = params['taskId'];
      this.projectService.getTasksByProjectId(this.tmpProjectId).subscribe((data) => {
        var tmpdata: any = data;
        for(let entry of tmpdata){
          if(entry.id == this.tmpTaskId){
            this.tmpTask = entry;
            this.getComments(this.tmpTaskId);
          }
          else
          {
            this.projectService.getArchivedTasksByProjectId(this.tmpProjectId).subscribe((data) => {
              var tmpdata: any = data;
              for(let entry of tmpdata){
                //pokud nenalezne, tak něco
                if(entry.id == this.tmpTaskId){
                  this.tmpTask = entry;
                }
              }
            })
          }
        }
      })
      this.getProjectInfo(this.tmpProjectId);
      //this.tmpTask = [];      
    });
  }

  getProjectInfo(prjctId: number)
  {
    this.projectService.getProjectByItsId(prjctId)
      .subscribe((data) =>
      {
        this.tmpProject = data;
        if (this.tmpProject.owner_Id == this.tmpUserId) this.isOwner = true;
        else this.isOwner = false;
      });
  }

  deleteProject(prjctId: number)
  {
    var confirmAnswer = confirm("Are you sure you want to delete project " + this.tmpProject.name);
    if (confirmAnswer)
    {
      this.projectService.deleteProject(prjctId).subscribe((data) =>
      {
        this.router.navigate([""]);
      }), error => console.error(error);
    } 
  }

  //comment adding
  addComment()
  {
    if (!this.commentForm.valid)
    {
      alert("Pro přidání komentáře je potřeba zadat nějaký text.");
      return;
    }
    this.commentForm.controls['TimeChanged'].setValue(Date.now);
    this.commentForm.controls['Fk_Owner_Id'].setValue(this.tmpUserId);
    this.commentForm.controls['Fk_Task_Id'].setValue(this.tmpTaskId);
    console.log(this.commentForm.value);
    this.commentService.saveComment(this.commentForm.value).subscribe((data) => 
    {
      this.getComments(this.tmpTaskId);
    }, error => 
    { 
      console.log(error);
    })
  }

  //ziskani komentařů k danému ukolu
  getComments(taskID: number)
  {
    this.commentService.getComments(taskID).subscribe((data) => 
    {
      this.tmpCommentsList = data;
    })
  }

  //Task things
  //Zobrazit formulář pro přidání ukolu
  showTaskForm()
  {
    this.taskForm.controls['Name'].setValue("");
    this.taskForm.controls['Description'].setValue("");
    this.isEditing = false;
    this.isVisible = !this.isVisible;
  }

  //Method to show edit task form and darken background 
  showEditTaskForm()
  {
    this.taskForm.controls['Name'].setValue(this.tmpTask.name);
    this.taskForm.controls['Description'].setValue(this.tmpTask.description);
    this.isEditing = true;
    this.isVisible = !this.isVisible;
  }


  //Create Task - max length 30
  createTask()
  {
    if (!this.taskForm.valid)
    {
      alert("Nesprávný formát názvu, maximální délka nesmí přesáhnout 30 znaků.");
      return;
    }
    this.taskForm.controls['Fk_Owner_Id'].setValue(this.tmpUserId);
    this.taskForm.controls['Fk_Project_Id'].setValue(this.tmpProjectId);
    this.projectService.saveTask(this.taskForm.value)
      .subscribe((data) =>
      {
        alert("Task created!");
        this.isVisible = false;
        //this.getTasks();
      }, error =>
      {
        alert("There was problem with creating a task!");
      })
  }

  //Method for editing a task
  editTask()
  {
    if(!this.taskForm.valid)
    {
      alert("Invalid title of the task!");
      return;
    }
    this.taskForm.controls['Fk_Owner_Id'].setValue(this.tmpUserId);
    this.taskForm.controls['Fk_Project_Id'].setValue(this.tmpProjectId);
    this.projectService.editTask(this.taskForm.value, this.tmpTask.id)
    .subscribe((data) =>
    {
      alert("Task edited!");
      this.isVisible = false;
      //this.getTasks();
    }, error =>
    {
      alert("There was problem with creating a task!");
    })
  }

  //Method for archiving a task
  archiveTask()
  {
    var confirmAnswer = confirm("Jste si jistí, že chcete archivovat úkol s názvem \"" + this.tmpTask.name + "\" a ID \"" + this.tmpTask.id + "\"?");
    if (confirmAnswer) 
    {
      this.projectService.archiveTask(this.tmpTask.id).subscribe((data) => 
      {
        alert("Úkol byl archivován.");
        //this.getUsers();
      },
      error => console.error(error))
    } 
  }

  //Method for deleting a task
  deleteTask() 
  {
    var confirmAnswer = confirm("Jste si jistí, že chcete smazat úkol s názvem \"" + this.tmpTask.name + "\" a ID \"" + this.tmpTask.id + "\"?");
    if (confirmAnswer) 
    {
      this.projectService.deleteTask(this.tmpTask.id).subscribe((data) => 
      {
        alert("Úkol byl odebrán ze systému.");
        //this.getUsers();
      },
      error => console.error(error))
    } 
  }

  //OnChange select option
  onChangeTask(value){
    this.projectService.editTaskStatus(value, this.tmpTask.id).subscribe((data) =>
    {
      //do smth
    })
  }
}

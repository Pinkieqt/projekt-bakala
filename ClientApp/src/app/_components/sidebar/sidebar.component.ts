import { Component, OnDestroy } from '@angular/core';
import { ProjectService } from '../../_services/project.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnDestroy {

  private tmpProjectId: number;
  private taskList: any;
  private archivedTaskList: any;


  constructor(
    private prjctService: ProjectService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  )
  {
    this.activatedRoute.params.subscribe(params =>
      {
        this.tmpProjectId = params['id'];
        localStorage.setItem("projectId", params['id']);
        this.getTasks();
           
      });
  }

  ngOnDestroy() {
    this.tmpProjectId = null;
    this.taskList = null;
  }

  //Not archived tasks
  getTasks()
  {
    this.prjctService.getTasksByProjectId(this.tmpProjectId).subscribe(data => this.taskList = data);
  }

  //Get archived tasks


  //Checkbutton event
  checkValue(event)
  {
    if(event.target.checked)
    {
      this.prjctService.getArchivedTasksByProjectId(this.tmpProjectId).subscribe(data => this.archivedTaskList = data);
    }
    else
    {
      this.archivedTaskList = [];
    }
 }
}

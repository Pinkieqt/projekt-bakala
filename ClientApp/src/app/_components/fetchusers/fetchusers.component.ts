import { Component } from '@angular/core';
import { UserService } from '../../_services/user.service';

@Component({
  selector: 'app-fetchusers',
  providers: [UserService],
  templateUrl: './fetchusers.component.html',
  styleUrls: ['./fetchusers.component.css']
})

export class FetchusersComponent{
  public userList: any;

  constructor(private userService: UserService) {
    this.getUsers();
  }

  getUsers() {
    this.userService.getUsers().subscribe(data => this.userList = data)
  }

  deleteUser(login: string, id: number) {
    if(id == 1){
      alert("You cannot delete user with administrator rights!");
    }
    else
    {
      var confirmAnswer = confirm("Are you sure that you want to delete user with login: " + login);
      if (confirmAnswer) {
        this.userService.deleteUser(login).subscribe((data) => {
          this.getUsers();
        }),
        error => {
          alert("Nemáte práva toto udělat!");
          console.error(error);
        }
      } 
    }
   
  }
  
}

interface UserData {
  id: number;
  login: string;
  password: string;
  first_name: string;
  last_name: string;
  email: string;
}
